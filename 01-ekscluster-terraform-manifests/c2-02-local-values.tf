# Define Local Values in Terraform
locals {
  owners = var.company
  environment = var.environment
  name = "${var.company}-${var.environment}"
  #name = "${local.owners}-${local.environment}"
  common_tags = {
    owners = local.owners
    environment = local.environment
  }
  eks_cluster_name = "${local.name}-${var.cluster_name}"  
} 