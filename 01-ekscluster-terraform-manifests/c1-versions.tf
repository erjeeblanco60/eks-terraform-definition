# Terraform Settings Block
terraform {
  required_version = ">= 1.0.0"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      #version = "~> 4.12"
      version = ">= 4.65"
     }
  }
  # Adding Backend as S3 for Remote State Storage
  backend "s3" {
    bucket = "<s3-bucket-name>"
    key    = "<s3-bucket-key>"
    region = "<aws-region>" 
 
    # For State Locking
    dynamodb_table = "<dynamodb-table-name>"  
  }   
}

# Terraform Provider Block
provider "aws" {
  region = var.aws_region
}