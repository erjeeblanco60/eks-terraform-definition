# Terraform Settings Block
terraform {
  required_version = ">= 1.0.0"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      #version = "~> 4.12"
      version = ">= 4.65"
     }
    helm = {
      source = "hashicorp/helm"
      #version = "2.5.1"
      #version = "~> 2.5"
      version = ">= 2.9"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      #version = "~> 2.11"
      version = ">= 2.20"
    }      
  }
  # Adding Backend as S3 for Remote State Storage
  backend "s3" {
    bucket = "<s3-bucket-name>"
    key    = "<s3-bucket-key>"
    region = "<aws-region>" 
 
    # For State Locking
    dynamodb_table = "<dynamodb-table-name>"  
  }     
}

# Terraform AWS Provider Block
provider "aws" {
  region = var.aws_region
}

