# Terraform Remote State Datasource - Remote Backend AWS S3
data "terraform_remote_state" "eks" {
  backend = "s3"
  config = {
    bucket = "<s3-bucket-name>"
    key    = "<s3-bucket-key>"
    region = var.aws_region
  }
}