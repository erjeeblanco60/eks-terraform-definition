# Terraform Infrastructure on Kubernetes Deployment

## Introduction

This document provides an overview of the Terraform infrastructure deployment on Kubernetes. The deployment is organized within the `01-ekscluster-terraform-manifests` directory and involves the creation of an EKS cluster using Terraform.

### Terraform Backend and Remote State Storage

The deployment utilizes a Terraform backend for managing remote state storage. The remote state is stored in an S3 bucket named "tfstate-4-v2." Additionally, for state locking, a DynamoDB table named "tfstate-4-lock" is used to prevent concurrent modifications.

### Configuration Variables and Local Values

To configure the deployment, several variables are defined in the `c2-01-generic-variables.tf` file. These variables include `aws_region`, `environment`, and `company`. They allow for customization of the infrastructure based on specific requirements.

The `c2-02-local-values.tf` file contains local values defined in Terraform.

These local values are used to generate the names and tags for various resources within the deployment.

### VPC Configuration

The VPC (Virtual Private Cloud) configuration is handled in two files. The `c3-01-vpc-variables.tf` file contains the variables necessary for VPC setup, while the `c3-02-vpc-module.tf` file defines the VPC Terraform module.

The VPC Terraform module provides a reusable and modular approach to defining VPC resources, such as subnets, route tables, and security groups. It simplifies the process of creating and managing VPC infrastructure within the deployment.

### EKS Manifest Deployment

Continuing with the Terraform infrastructure deployment on Kubernetes, the following section covers the EKS manifest, which includes various Terraform files from `c5-01` to `c5-08`. These files define the necessary resources for setting up and configuring an EKS cluster.

#### EKS Variables and Outputs

The `c5-01-eks-variables.tf` file contains variables specific to the EKS cluster. These variables allow for customization of the cluster's configuration, such as the cluster name, instance types, desired capacity, and more.

On the other hand, the `c5-02-eks-output.tf` file defines outputs that provide relevant information about the EKS cluster once it's provisioned. These outputs can be used for reference or to pass values to other parts of the deployment.

#### IAM Roles for EKS Cluster and Node Group

In order to properly configure permissions and access control for the EKS cluster and its associated node group, two IAM roles are defined: `iamrole-for-eks-cluster` and `iamrole-for-eks-nodegroup`. These roles ensure that the EKS cluster and the worker nodes have the necessary permissions to function properly within the AWS environment.

#### EKS Cluster Resource Manifest

The `c5-06-eks-cluster.tf` file contains the main resource manifest for provisioning the EKS cluster. It defines the EKS cluster itself, specifying the version, name, VPC configuration, and other relevant settings. This resource manifest ensures the creation and proper configuration of the EKS cluster.

#### EKS Node Group (Private)

To support the EKS cluster with worker nodes, the `c5-08-eks-node-group-private.tf` file defines an EKS node group. This node group creates worker nodes within private subnets of the VPC, enhancing security and enabling private communication among the nodes.

These Terraform files work in conjunction to create an EKS cluster with the necessary resources, IAM roles, and node groups. They ensure a well-configured and secure Kubernetes environment within the AWS infrastructure.

### IAM OIDC Connect Provider

Within the Terraform infrastructure deployment on Kubernetes, two files, `c6-01-iam-oidc-connect-provider-variables.tf` and `c6-02-iam-oidc-connect-provider.tf`, define the IAM OIDC Connect Provider. Let's explore the purpose and significance of this code.

#### `c6-01-iam-oidc-connect-provider-variables.tf`

The `c6-01-iam-oidc-connect-provider-variables.tf` file defines a Terraform variable named `eks_oidc_root_ca_thumbprint`. This variable represents the thumbprint of the Root CA (Certificate Authority) for EKS OIDC (OpenID Connect). It is a string type variable with a default value of "9e99a48a9960b14926bb7f3b02e22da2b0ab7280".

The OIDC Root CA thumbprint is used to establish trust between AWS IAM (Identity and Access Management) and the Kubernetes cluster running on EKS (Elastic Kubernetes Service). This thumbprint ensures the integrity and security of the communication between the two entities.

#### AWS IAM OpenID Connect Provider in Kubernetes, AWS, and Terraform

AWS IAM OpenID Connect (OIDC) Provider is a feature that enables Kubernetes clusters running on AWS EKS to authenticate and authorize AWS IAM users, roles, and groups to access the cluster. This integration allows you to leverage AWS IAM's fine-grained access control capabilities to manage access to your EKS cluster.

In the context of Terraform, the `c6-02-iam-oidc-connect-provider.tf` file sets up the AWS IAM OIDC Connect Provider. This provider establishes the trust relationship between AWS IAM and the Kubernetes cluster. It utilizes the `eks_oidc_root_ca_thumbprint` variable defined in `c6-01-iam-oidc-connect-provider-variables.tf` to specify the thumbprint of the Root CA for EKS OIDC.

By configuring the IAM OIDC Connect Provider, you enable Kubernetes to recognize AWS IAM identities and their associated permissions. This allows you to authenticate and authorize IAM users, roles, and groups to access and interact with your EKS cluster.

Overall, the IAM OIDC Connect Provider facilitates seamless integration between AWS IAM and Kubernetes, providing a unified identity and access management solution within the Kubernetes deployment on EKS.

### Kubernetes Provider and Kubernetes ConfigMap

Continuing with the Terraform infrastructure deployment on Kubernetes, let's discuss the `c07-01-kubernetes-provider.tf` and `c07-02-kubernetes-configmap.tf` files, which involve the Kubernetes provider and the Kubernetes ConfigMap, respectively.

#### `c07-01-kubernetes-provider.tf`

The `c07-01-kubernetes-provider.tf` file is responsible for configuring the Kubernetes provider in Terraform. The Kubernetes provider allows Terraform to interact with a Kubernetes cluster, enabling the management and provisioning of Kubernetes resources.


Here, the provider is configured with details such as the cluster context, cluster host, client certificate, client key, cluster CA certificate, and token. These configurations allow Terraform to connect to and authenticate with the Kubernetes cluster.

The Kubernetes provider enables Terraform to manage resources within the Kubernetes environment, such as deploying and managing Kubernetes objects like pods, services, deployments, and more.

#### `c07-02-kubernetes-configmap.tf`

The `c07-02-kubernetes-configmap.tf` file is used to define and provision a Kubernetes ConfigMap. A ConfigMap is a Kubernetes object that provides a way to store configuration data that can be consumed by containers running in pods.

This file typically includes the definition of the ConfigMap resource using Terraform syntax, along with the required data. The ConfigMap may contain key-value pairs or configuration files that are accessible to the containers within the Kubernetes cluster.


In this example, a ConfigMap named "example-configmap" is created in the "default" namespace with two key-value pairs.

By utilizing the Kubernetes provider and configuring a ConfigMap, Terraform enables the management of Kubernetes resources and the provisioning of configuration data that can be accessed by the containers running in the Kubernetes cluster.

### IAM Admin User and IAM Basic User

In the Terraform infrastructure deployment on Kubernetes, the files `c08-01-iam-admin-user.tf` and `c08-02-iam-basic-user.tf` focus on creating IAM (Identity and Access Management) users with different levels of access.

#### `c08-01-iam-admin-user.tf`

The `c08-01-iam-admin-user.tf` file is responsible for creating an IAM Admin user. This user is granted full access to AWS resources. Typically, an admin user has extensive privileges and can perform any action within the AWS account.

The configuration for the IAM Admin user may include an IAM user resource and an associated IAM policy that grants full access

#### `c08-02-iam-basic-user.tf`

On the other hand, the `c08-02-iam-basic-user.tf` file is responsible for creating an IAM Basic user. This user has more restricted access compared to the Admin user.

The configuration for the IAM Basic user might include an IAM user resource and an associated IAM policy that specifies a limited set of permissions. the IAM Basic user is granted permission to perform actions such as listing roles, managing EKS (Elastic Kubernetes Service) resources, and retrieving SSM (Systems Manager) parameters. These permissions are specified in the IAM policy assigned to the Basic user.

These two sets of IAM user configurations provide different levels of access within the AWS account, with the Admin user having full access and the Basic user having limited access to specific resources.

### IAM EKS Admin Role and Group Membership

In the Terraform infrastructure deployment on Kubernetes, the files `c09-01-eksadmin-role.tf` and `c09-02-eksadmins-iam-group.tf` involve the creation of an IAM EKS Admin role and the configuration of group membership.

#### `c09-01-eksadmin-role.tf`

The `c09-01-eksadmin-role.tf` file is responsible for defining an IAM role for the EKS (Elastic Kubernetes Service) admin. This role is specifically tailored for managing and administering the EKS cluster.

The configuration for the EKS admin role may involve an IAM role resource withthe necessary permissions and an associated IAM policy. The policy may grant permissions such as assuming the EKS admin role and performing specific actions related to EKS.

### `c09-02-eksadmins-iam-group.tf`

The `c09-02-eksadmins-iam-group.tf` file handles the creation of an IAM group named "eksadmins" and the configuration of group membership for the EKS admin users.

The configuration typically includes an IAM group resource, an IAM group policy specifying the permissions, an IAM user resource, and the association of the user with the group.

With this configuration, an IAM group named "eksadmins" is created, and an IAM group policy is associated with it, granting permission to assume the EKS admin role. Additionally, an IAM user named "eksadmin" is created and added as a member of the "eksadmins" group.

This setup allows the specified IAM user to assume the EKS admin role and perform administrative tasks within the EKS cluster.

By organizing the IAM configurations into roles, groups, and users, we can effectively manage and control access to the EKS cluster within your Terraform infrastructure deployment.


### AWS IAM Role - EKS Read-Only User

The `C10-01-iam-role-eksreadonly.tf` file is responsible for creating an AWS IAM Role for the EKS Read-Only User. This role is designed to provide read-only access to the EKS (Elastic Kubernetes Service) cluster.

The configuration for the IAM Role may include an IAM role resource, an IAM policy document that defines the permissions for the role, and an attachment of the policy to the role.

 an IAM Role named "eks-readonly-role" is created, along with a policy that grants read-only permissions for EKS-related operations and EC2 describe actions. The policy is attached to the IAM Role using the `aws_iam_role_policy_attachment` resource.

This IAM Role can be assumed by users or services, granting them read-only access to EKS resources within the AWS account.

### IAM Group and User for EKS Read-Only

The `C10-02-iam-group-and-user-eksreadonly.tf` file deals with the creation of an IAM group and user for the EKS Read-Only access.

This configuration typically includes an IAM group resource, an IAM user resource, and the association of the user with the group.
 
 An IAM group named "eks-readonly-group" is created, along with an IAM user named "eks-readonly-user." The user is then added as a member of the group using the `aws_iam_group_membership` resource.

This setup allows users within the "eks-readonly-group" to inherit the permissions assigned to the group. Users belonging to this group will have read-only access to the EKS cluster.

### Cluster Role and EKSReadOnly Cluster Role

In the `C10-03-k8s-clusterrole-clusterrolebinding.tf` file, the Cluster Role and EKSReadOnly Cluster Role Binding are defined.

#### Cluster Role

A Cluster Role is a Kubernetes role that defines a set of permissions or access rights within a Kubernetes cluster. It specifies the actions a user or service account can perform on Kubernetes resources.

The Cluster Role configuration typically includes the definition of the Cluster Role resource and the associated rules that define the allowed actions and resources.

a Cluster Role named "example-cluster-role" is defined. It allows the actions `get`, `list`, and `watch` on resources such as pods and services. The `api_groups` field is set to an empty string, indicating that the resources are part of the core API group.

Cluster Roles can be bound to users, groups, or service accounts in order to grant them the defined permissions within the Kubernetes cluster.

#### EKSReadOnly Cluster Role and Cluster Role Binding

The `eksreadonly_clusterrole` and `eksreadonly_clusterrolebinding` are specific Cluster Role and Cluster Role Binding defined for the EKS Read-Only user.

The EKSReadOnly Cluster Role typically includes a similar configuration to the Cluster Role but with permissions specifically tailored for read-only access to EKS resources. The Cluster Role Binding associates the EKSReadOnly Cluster Role with the EKS Read-Only user or service account.

The exact configuration of the EKSReadOnly Cluster Role and Cluster Role Binding may vary depending on your specific requirements and the desired read-only permissions for the EKS Read-Only user.

By configuring the Cluster Role and Cluster Role Binding, you can define granular permissions for accessing and interacting with resources within the Kubernetes cluster, in this case, specifically for the EKS Read-Only user.

### IAM Role for EKS Developer

In the `c11-01-iam-role-eksdeveloper.tf` file, an IAM Role for the EKS Developer is defined. This role provides the necessary permissions and access for developers working with the EKS (Elastic Kubernetes Service) cluster.

The IAM Role configuration may include an IAM role resource and an associated IAM policy document that specifies the allowed actions and resources.

With this configuration, an IAM Role named "eks-developer-role" is created, along with a policy that grants permissions specific to EKS developer access. The policy includes actions such as listing roles, retrieving SSM parameters, describing EKS node groups, and accessing the Kubernetes API, among others.

### IAM Group and User for EKS Developer

The `c11-02-iam-group-and-user-eksdeveloper.tf` file is responsible for creating an IAM group and user specifically for EKS developers.

The configuration typically includes an IAM group resource, an IAM user resource, and the association of the user with the group.

an IAM group named "eks-developer-group" is created, along with an IAM user named "eks-developer-user." The user is then added as a member of the group using the `aws_iam_group_membership` resource.

This setup allows users within the "eks-developer-group" to inherit the permissions assigned to the group. Users belonging to this group will have the necessary permissions to perform EKS development-related actions.

### Cluster Role and Cluster Role Binding for EKS Developer

In the `c11-03-k8s-clusterrole-clusterrolebinding.tf` file, the Cluster Role and Cluster Role Binding for the EKS Developer are defined.

#### Cluster Role

The Cluster Role for the EKS Developer is a Kubernetes role that specifies the permissions or

access rights within a Kubernetes cluster for developers working with the EKS cluster.

The configuration typically includes the definition of the Cluster Role resource and the associated rules that define the allowed actions and resources.

a Cluster Role named "eksdeveloper-clusterrole" is defined. It allows the actions `get` and `list` on resources such as nodes, namespaces, pods, events, and services. Additional rules are specified for apps and batch-related resources.

The Cluster Role defines the specific permissions that EKS developers have within the Kubernetes cluster.

#### Cluster Role Binding

The Cluster Role Binding associates the Cluster Role with the EKS Developer user or group, granting them the defined permissions within the Kubernetes cluster.

The Cluster Role Binding associates the Cluster Role "eksdeveloper-clusterrole" with the group "eks-developer-group" as the subject. This ensures that members of the "eks-developer-group" have the defined permissions within the Kubernetes cluster.

By defining the IAM Role, Group, and associated Kubernetes Cluster Role and Cluster Role Binding, you provide EKS developers with the necessary permissions and access to work effectively within the EKS cluster.

### Installing Load Balancer Controller

In the `02-lbc-install-terraform-manifests` file, the installation of the Load Balancer Controller (LBC) is performed. The Load Balancer Controller is a component that manages and configures load balancers in a Kubernetes cluster.

To facilitate the installation process, the Terraform configuration utilizes the following elements:

#### Remote State Data Source

The `terraform_remote_state` data source retrieves information from a remote state file stored in an S3 bucket. In this case, the remote state is sourced from the bucket named "tfstate-4-v2" in the specified AWS region. The remote state file is located at "dev/eks-cluster/terraform.tfstate".

By utilizing the remote state data source, the configuration can access information stored in the Terraform state file of the EKS cluster, which may be necessary for the Load Balancer Controller installation.

#### AWS Load Balancer Controller IAM Policy

The `data "http" "lbc_iam_policy"` block retrieves the AWS Load Balancer Controller IAM policy. It fetches the policy file directly from the specified URL. The policy defines the necessary permissions for the Load Balancer Controller to interact with AWS resources.

By retrieving the IAM policy from the provided URL, the configuration can ensure that the Load Balancer Controller has the required permissions to operate within the AWS environment.

#### Creating AWS Load Balancer Controller IAM Policy

After fetching the AWS Load Balancer Controller IAM policy, it can be created within the AWS environment to grant the necessary permissions.

The exact configuration for creating the policy is not provided in the file, but it would typically involve defining an `aws_iam_policy` resource and specifying the policy document using the fetched IAM policy data.

#### Helm Provider for ALB Ingress Controller Installation

The Helm provider is utilized to install the ALB (Application Load Balancer) Ingress Controller. The ALB Ingress Controller is responsible for configuring and managing AWS Application Load Balancers for routing external traffic into the Kubernetes cluster.

The specific configuration for installing the ALB Ingress Controller using Helm is not provided in the file. However, it would involve using the `helm` provider and specifying the necessary values and charts to deploy the Ingress Controller.

#### Creating Ingress Class for Backend Node.js

The configuration also includes creating an Ingress Class specifically for the backend Node.js application. An Ingress Class defines a set of rules for routing incoming requests to the backend services based on the specified criteria.

The details of creating the Ingress Class for the backend Node.js application are not provided in the file. However, it would typically involve defining an `kubernetes_ingress_class` resource and specifying the necessary settings for routing traffic to the Node.js backend.

Overall, the `02-lbc-install-terraform-manifests` file combines various Terraform resources and data sources to facilitate the installation and configuration of the Load Balancer Controller and related components within the Kubernetes cluster.

### Installing ExternalDNS

In the `03-externaldns-install-terraform-manifests` file, the installation of ExternalDNS is performed. ExternalDNS is a Kubernetes controller that automatically configures DNS providers to create DNS records for Kubernetes services.

To facilitate the installation process, the Terraform configuration utilizes the following elements:

#### Remote State Data Source

Similar to the previous file, the `terraform_remote_state` data source is used to retrieve information from a remote state file stored in an S3 bucket. This allows access to the necessary information from the EKS cluster's Terraform state file.

#### Creating ExternalDNS IAM Policy

The configuration typically includes creating an IAM policy for ExternalDNS based on the fetched IAM policy document. The IAM policy is created using an `aws_iam_policy` resource and specifying the policy document using the fetched IAM policy data.

#### Helm Provider for ExternalDNS Installation

The Helm provider is utilized to install ExternalDNS. Helm is a package manager for Kubernetes that simplifies the deployment and management of applications. The Helm provider allows Terraform to interact with Helm charts and install ExternalDNS.

The specific configuration for installing ExternalDNS using Helm is not provided in the file. However, it would involve using the `helm` provider and specifying the necessary values and charts to deploy ExternalDNS.

Overall, the `03-externaldns-install-terraform-manifests` file combines various Terraform resources and data sources to facilitate the installation and configuration of ExternalDNS within the Kubernetes cluster. It fetches the necessary IAM policy for ExternalDNS, creates the IAM policy, and uses the Helm provider to install ExternalDNS. These configurations automate the setup of ExternalDNS, enabling automatic DNS record management for Kubernetes services.